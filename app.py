import sys
import math
import numpy as np
import pandas as pd

from PyQt5.QtWidgets import (
    QApplication, QMainWindow, QFileDialog
)
from PyQt5.uic import loadUi
from PyQt5.QtSerialPort import QSerialPortInfo

from ui_temp_sensor import Ui_MainWindow

from SerialComCtrlQt import SerialCtrl
from ui_functions import *

class Window(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        
        # run some functions at startup
        self.connectUI()
        self.refresh_ports()
        self.clear_graph()
        
        self.arduino = SerialCtrl()

    def connectUI(self):
        # define some signals
        self.stop_requested = 0 # allows user to pause serial read

        # connect buttons to functions
        self.button_export.clicked.connect(self.export_CSV)
        self.button_connect.clicked.connect(self.connect_serial)
        self.button_read.clicked.connect(self.read_serial)
        self.button_stop.clicked.connect(self.stop_serial)

        # connect actions to functions
        self.actionExport.triggered.connect(self.export_CSV)
        self.actionConnect.triggered.connect(self.connect_serial)
        self.actionDisconnect.triggered.connect(self.disconnect_serial)
        self.actionClear.triggered.connect(self.clear_graph)
        self.actionRefresh_ports.triggered.connect(self.refresh_ports)
        self.actionExit.triggered.connect(self.close_window)

        # user-friendly enable/disable buttons in GUI 
        self.button_stop.setEnabled(0)
        self.button_read.setEnabled(0)

    def close_window(self):
        print("Disconnecting serial...")
        self.disconnect_serial()
    
        print("Closing application...")
        QApplication.quit()

    def refresh_ports(self):
        self.dropdown_port.clear()
        serialPortInfos = QSerialPortInfo.availablePorts()
        for portInfo in serialPortInfos:
            self.dropdown_port.addItem(portInfo.portName())
            print(portInfo.portName(), ":", portInfo.description())

    def clear_graph(self):
        self.widget_graph.canvas.ax.clear()
        self.widget_graph.canvas.ax.set_xlabel("Time")
        self.widget_graph.canvas.ax.set_ylabel("Temperature [C]")
        self.widget_graph.canvas.draw()

        self.x = []
        self.y = []

    def export_CSV(self):
        filename = QFileDialog.getSaveFileName()
        print("Exporting CSV to", filename[0])
        
        df = pd.DataFrame(data={"Time": self.x, "Temperature [C]": self.y})
        df.to_csv(filename[0], sep=',',index=False)

        print("Done.")
        
    def connect_serial(self):
        port = self.dropdown_port.currentText()
        baud = self.dropdown_baud.currentText()
        print("Connecting over serial to device on port", port, "with rate", baud)

        status = self.arduino.serialOpen(port, baud)
        if status:
            self.button_connect.setEnabled(0)
            self.button_connect.setText("Connected")
            self.actionConnect.setEnabled(0)
            self.button_read.setEnabled(1)

    def disconnect_serial(self):
        status = self.arduino.serialClose()
        if not status:
            self.button_connect.setEnabled(1)
            self.button_connect.setText("Connect")
            self.button_read.setEnabled(0)
            self.actionConnect.setEnabled(1)

    def read_serial(self):
        self.stop_requested = 0
        self.button_read.setEnabled(0)
        self.button_stop.setEnabled(1)
        
        while not self.stop_requested:
            QApplication.processEvents() # stops GUI hanging
            message = self.arduino.serialRead()
            
            if message != None:
                update_graph(self, message, self.x, self.y)

    def stop_serial(self):
        self.stop_requested = 1
        self.button_stop.setEnabled(0)
        self.button_read.setEnabled(1)
        
if __name__ == "__main__":
    app = QApplication(sys.argv)
    win = Window()
    win.show()
    sys.exit(app.exec())

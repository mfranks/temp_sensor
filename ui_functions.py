import time

# example of how to keep functions tidy, in an external file

def update_graph(self, message, x, y):
    t = time.strftime("%H:%M:%S", time.localtime())
    
    self.widget_graph.canvas.ax.clear()
    self.widget_graph.canvas.ax.set_xlabel("Time")
    self.widget_graph.canvas.ax.set_ylabel("Temperature [C]") 

    try:
        x += [t]
        y += [float(message[0:5])]
    
        self.widget_graph.canvas.ax.plot(x, y, color="r")
            
        Nx = len(x)
            
        if Nx >= 2:
            # time is not automatically formatted on matplotlib axes
            ticks = [x[0], x[-1]] # take first and last times
            
            # label leftmost and rightmost x axis ticks
            self.widget_graph.canvas.ax.set_xticks([0,Nx])
            self.widget_graph.canvas.ax.set_xticklabels(ticks)
            
        self.widget_graph.canvas.draw()
        
        print(Nx,t,": ",message[0:5])
    except ValueError:
        # sometimes value read from Arduino is just '\x00\x00\x00\x00\x00'
        print("Error converting string read from Arduino to float. Try disconnecting and reconnecting Arduino.")

int ThermistorPin = 0;
int Vo;
float R1 = 1091.9; // measured using a multimeter
float logR2, R2, T;
//float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;

// calculated from here: https://www.sterlingsensors.co.uk/pt1000-resistance-table
// using these formulae: https://www.servo.jp/member/admin/document_upload/AN144-Thermistor-Steinhart-Hart-Coefficients.pdf
float c = 0.0000592664367348686;
float b = -0.011955482226601;
float a = 0.0667213740258914;

float movingAverage(float value) {
  // stolen from here: https://stackoverflow.com/questions/67208086/how-can-i-calculate-a-moving-average-in-arduino
  
  const byte nvalues = 32;            // Moving average window size

  static byte current = 0;            // Index for current value
  static byte cvalues = 0;            // Count of values read (<= nvalues)
  static float sum = 0;               // Rolling sum
  static float values[nvalues];

  sum += value;

  // If the window is full, adjust the sum by deleting the oldest value
  if (cvalues == nvalues)
    sum -= values[current];

  values[current] = value;          // Replace the oldest with the latest

  if (++current >= nvalues)
    current = 0;

  if (cvalues < nvalues)
    cvalues += 1;

  return sum/cvalues;
}

void setup() {
  Serial.begin(9600);
}

void loop() {
  // stolen from here: https://www.circuitbasics.com/arduino-thermistor-temperature-sensor-tutorial/
  Vo = analogRead(ThermistorPin);
  R2 = R1 * (1023.0 / (float)Vo - 1.0);
  logR2 = log(R2);
  T = (1.0 / (a + b*logR2 + c*logR2*logR2*logR2));
  T = T - 273.15;
  //T = (T * 9.0)/ 5.0 + 32.0; 

  Serial.println(movingAverage(T));

  delay(1000);
}
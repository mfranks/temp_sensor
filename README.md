# temp_sensor

temp_sensor is a GUI for connecting to and reading from a temperature sensor, constructed using an Arduino. The GUI was designed as an example for how to design a basic GUI in Python using Qt, with features such as serial connection, live plotting to a graph, exporting data to CSV, and processing events outside of the main loop to ensure that the GUI does not freeze. This is not intended to be an accurate scientific instrument, but rather an educational exercise for anyone to use as a starting point, and to become familiar with designing GUIs for scientific experiments.
 
## Installation

Assuming git is installed on your system, navigate to the folder where you would like to 'install' the program and run the command:

```bash
git clone git@gitlab.phys.ethz.ch:mfranks/temp_sensor.git
```
Or you can simply download the compressed source code using the Code dropdown button.

## Usage

Assuming Python is already installed on your system (along with the necessary libraries), run:

```bash
cd temp_sensor
python app.py
```

The application window will open.

## Constructing the temperature sensor

The temperature sensor used to develop this project was constructed following [this guide](https://www.circuitbasics.com/arduino-thermistor-temperature-sensor-tutorial/). Due to equipment availability, a PT1000 thermistor was used in series with a 1.1 kOhm resistor, along with an Arduino NANO Every. Instructions for how the Steinhart-Hart coefficients were calculated are given as comments in the Arduino INO file.

## Arduino code

Flash the INO file in the Arduino/temp_sensor/ folder to an Arduino using the Arduino IDE.

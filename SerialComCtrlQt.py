from PyQt5 import QtCore, QtWidgets, QtSerialPort
from PyQt5.QtCore import QIODevice

# Originally written by T. Weber (ETH Zurich) for Tk
# but modified by M. Franks (ETH Zurich) to work with Qt

class SerialCtrl(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(SerialCtrl, self).__init__(parent)
    
    def serialOpen(self, port, baud):
        # attempt to open serial
        self.serial = QtSerialPort.QSerialPort(port, baudRate=QtSerialPort.QSerialPort.Baud9600)
        
        self.serial.open(QIODevice.ReadWrite)
        status = self.serial.isOpen()
        if (status):
            print("Connected.")
        else:
            print("Unable to connect.")
      
        return status

    def serialClose(self):
        try:
            self.serial.close()
            status = self.serial.isOpen()
            if (status):
                print("Arduino still connected.")
            else:
                print("Arduino disconnected.")
            
            return status
        
        except (AttributeError, UnboundLocalError):
            print("User must attempt connection first. More intelligent connect/disconnect to be implemented.")

    '''
    def serialWrite (self, command):
        print("writing to serial.")
        if isinstance(command, str):
            self.serial.write(bytes(self.encodeDataString(command),'utf-8'))

        if isinstance(command, list):
            self.serial.write(bytes(self.encodeDataArray(command),'utf-8'))
        #time.sleep(0.05)
        print("written to serial.")
    '''

    def serialRead (self):
        if self.serial.isOpen() and self.serial.bytesAvailable() >= 7:
            return self.serial.readLine().data().decode("utf-8").strip()
            #return self.serial.readLine()
        #else:
        #    return "no new data"

if __name__ == '__main__':
    SerialCtrl()
